/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/is-in-viewport.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/is-in-viewport.js":
/*!*******************************!*\
  !*** ./src/is-in-viewport.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nvar ImageLazyLoad = /*#__PURE__*/function () {\n  function ImageLazyLoad(selector) {\n    _classCallCheck(this, ImageLazyLoad);\n\n    this.selector = selector;\n    this.init();\n  }\n\n  _createClass(ImageLazyLoad, [{\n    key: \"init\",\n    value: function init() {\n      var _this = this;\n\n      document.addEventListener('DOMContentLoaded', function () {\n        var lazies = [].slice.call(document.querySelectorAll(_this.selector));\n\n        if (typeof window.IntersectionObserver !== 'function') {\n          console.log('Call to proper polifill');\n          lazies.forEach(function (lazy) {\n            if (lazy.getBoundingClientRect().top <= window.innerHeight && lazy.getBoundingClientRect().bottom >= 0 && getComputedStyle(lazy).display !== 'none') {\n              ImageLazyLoad.dispatchEvent(lazy);\n            }\n          });\n          return;\n        }\n\n        var lazyObserver = new IntersectionObserver(function (entries, observer) {\n          entries.forEach(function (entry) {\n            console.log(entry);\n\n            if (entry.isIntersecting) {\n              var lazy = entry.target;\n              ImageLazyLoad.dispatchEvent(lazy);\n              lazyObserver.unobserve(lazy);\n            }\n          });\n        });\n        lazies.forEach(function (lazy) {\n          lazyObserver.observe(lazy);\n        });\n      });\n    }\n  }], [{\n    key: \"dispatchEvent\",\n    value: function dispatchEvent(selector) {\n      var evt = new CustomEvent('elementObserved', {\n        detail: {\n          selector: selector\n        }\n      });\n      document.body.dispatchEvent(evt);\n    }\n  }]);\n\n  return ImageLazyLoad;\n}();\n\nwindow.ImageLazyLoad = ImageLazyLoad;\n/* harmony default export */ __webpack_exports__[\"default\"] = (ImageLazyLoad);\n\n//# sourceURL=webpack:///./src/is-in-viewport.js?");

/***/ })

/******/ });