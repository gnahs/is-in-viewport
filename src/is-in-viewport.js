class IsInViewport {
    constructor(selector, eventName = 'elementObserved') {
        this.selector = selector
        this.eventName = eventName
        this.events = {
            scrollFunction: this.scrollFunction.bind(this),
        }
        this.init()
    }

    init() {
        // document.addEventListener('DOMContentLoaded', () => {
            const lazies = [].slice.call(document.querySelectorAll(this.selector))
            if (typeof window.IntersectionObserver !== 'function') {
                window.addEventListener('scroll', this.events.scrollFunction, true)
                window.setTimeout(() => {
                    this.scrollFunction()
                }, 4)
                return
            }
            let options = {
                rootMargin: '50px',
            }

            const lazyObserver = new IntersectionObserver((entries, observer) => {
                entries.forEach((entry) => {
                    if (entry.isIntersecting) {
                        const lazy = entry.target
                        this.dispatchEvent(lazy)
                        lazyObserver.unobserve(lazy)
                    }
                })
            }, options)

            lazies.forEach((lazy) => {
                lazyObserver.observe(lazy)
            })
       //  }, false)
    }

    scrollFunction() {
        const lazies = [].slice.call(document.querySelectorAll(this.selector))
        // eslint-disable-next-line
        lazies.forEach((lazy) => {
            if ((lazy.getBoundingClientRect().top <= window.innerHeight && lazy.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazy).display !== 'none') {
                this.dispatchEvent(lazy)
            }
        })
    }

    dispatchEvent(selector) {
        const evt = new CustomEvent(this.eventName, {
            detail: {
                selector,
            },
        })
        document.body.dispatchEvent(evt)
    }
}

export default IsInViewport
