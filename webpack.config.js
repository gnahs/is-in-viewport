const webpack = require('webpack-boilerplate/webpack.config.js')

webpack.entry = {
    'image-lazy-load': 'js/src/is-in-viewport.js',
}

module.exports = webpack
